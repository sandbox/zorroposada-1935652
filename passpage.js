/**
 * @file
 * Passpage module, which allows administrators to configure passwords to access 
 * specific pages for anonymous users.
 * 
 * Pages could be node pages, panels pages, views pages, webforms, etc.
 * 
 */

(function ($) {
  Drupal.behaviors.passpage = {
    attach: function (context, settings) {
      
    }
  }
})(jQuery);
